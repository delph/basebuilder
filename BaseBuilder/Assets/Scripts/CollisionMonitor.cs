﻿using UnityEngine;
using System.Collections;

public class CollisionMonitor : MonoBehaviour
{
	[SerializeField]
	bool logCollisions = false;
	public event System.Action<Collision> OnEnter;
	public event System.Action<Collision> OnStay;
	public event System.Action<Collision> OnExit;

	// Note collision arugment can be excluded from these
	// methods to improve performance

	void OnCollisionEnter(Collision collision)
	{
		if (logCollisions)
		{
			Debug.Log("OnCollisionEnter: " + collision.collider.name, this);
		}
		if (OnEnter != null)
		{
			OnEnter(collision);
		}
	}

	void OnCollisionExit(Collision collision)
	{
		if (logCollisions)
		{
			Debug.Log("OnCollisionExit: " + collision.collider.name, this);
		}
		if (OnExit != null)
		{
			OnExit(collision);
		}
	}

	// Note collision arugment can be excluded to improve performance
	void OnCollisionStay(Collision collision)
	{
		if (OnStay != null)
		{
			OnStay(collision);
		}
	}
}
