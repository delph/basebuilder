﻿using UnityEditor;
using UnityEngine;
using System.Collections;

[CustomEditor(typeof(PatternBuilder))]
public class PatternBuilderEditor : Editor
{
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		var patternBuilder = target as PatternBuilder;

		GUILayout.Label("Pattern Visualisation");

		GUI.enabled = false;
		var pattern = patternBuilder.Pattern;
		for (int i = 0, l = pattern.Length; i < l; i++)
		{
			if(i%6 == 0)
			{
				EditorGUILayout.BeginHorizontal();
			}

			pattern[i] = EditorGUILayout.IntField(pattern[i]);

			if(i%6 == 5)
			{
				EditorGUILayout.EndHorizontal();
			}
		}
		GUI.enabled = true;
	}
}
