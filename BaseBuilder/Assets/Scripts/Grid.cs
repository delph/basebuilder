﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Vector2Int
{
	public Vector2Int(int x, int z)
	{
		this.X = x;
		this.Z = z;
	}
	public int X;
	public int Z;
}

public class Grid : MonoBehaviour
{
	float gridSize = 6f;
	Dictionary<int, Dictionary<int, Tile>> tiles;
	List<Tile> previewAffectedTiles = new List<Tile>();

	public float GridSize { get { return gridSize; } }

	void Awake()
	{
		tiles = new Dictionary<int, Dictionary<int, Tile>>();
		// Initialisation code - arguably should go on a GameController of some kind
		Object.FindObjectOfType<HUD>().Init(this, Object.FindObjectOfType<Camera>(), Object.FindObjectOfType<Terrain>());
	}

	public bool TryGetTile(int x, int z, out Tile tile)
	{
		tile = null;
		if(tiles.ContainsKey(x) && tiles[x].ContainsKey(z))
		{
			tile = tiles[x][z];
			return true;
		}
		return false;
	}

	public void RemoveTile(Tile tile)
	{
		Tile tileInGrid;
		int x = GetGridX(tile.transform), z = GetGridZ(tile.transform);
		if (TryGetTile(x, z, out tileInGrid) && tileInGrid == tile)
		{
			tiles[x].Remove(z);
			UpdateNeighbourAdjancency(tile);
			Object.Destroy(tile.gameObject);
		}
		else
		{
			Debug.LogErrorFormat("Provided tile does not match tile in grid at ({0}, {1})", x, z);
		}
	}

	public bool CanPlaceTile(Tile tile)
	{
		var tileSlot = new Vector2Int(GetGridX(tile.transform), GetGridZ(tile.transform));
		bool canPlace = true;
		if (TilesContainsKey(tileSlot))
		{
			canPlace = false;
		}
		else
		{
			var neighbours = GenerateNeighbours(tileSlot.X, tileSlot.Z);
			foreach(var key in neighbours.Keys)
			{
				if (TilesContainsKey(key))
				{
					if (!AttachPointsMatch(tile, tiles[key.X][key.Z], neighbours[key]))
					{
						canPlace = false;
						break;
					}
				}
			}
		}
		return canPlace;
	}

	private bool AttachPointsMatch(Tile tile1, Tile tile2, Direction directionFrom1to2)
	{
		var directionFrom2to1 = GetOppositeDirection(directionFrom1to2);
		return (tile1.GetAttachPoint(directionFrom1to2) == tile2.GetAttachPoint(directionFrom2to1));
	}

	public bool TryPlaceTile(Tile tile)
	{
		tile.transform.position = new Vector3(
			Mathf.Round(tile.transform.position.x / GridSize) * GridSize,
			0,
			Mathf.Round(tile.transform.position.z / GridSize) * GridSize);
		if(CanPlaceTile(tile))
		{
			Object.Destroy(tile.GridSnap);
			tile.transform.parent = this.transform;
			int x = GetGridX(tile.transform), z = GetGridZ(tile.transform);
			if(!tiles.ContainsKey(x))
			{
				tiles.Add(x, new Dictionary<int, Tile>());
			}
			tiles[x].Add(z, tile);
			tile.name = string.Format(tile.name + " ({0}, {1})", x, z);

			ClearPreviewAdjancency();
			tile.AddTileCollider(this.GridSize);
			tile.UpdateAdjacency(BuildAdjacency(tile));
			UpdateNeighbourAdjancency(tile);
			return true;
		}
		return false;
	}

	public void UpdatePreviewAdjancency(Tile previewTile)
	{
		var affectedTiles = new List<Tile>();
		UpdateNeighbourAdjancency(previewTile, affectedTiles, true);
		foreach(var tile in previewAffectedTiles)
		{
			if(!affectedTiles.Contains(tile))
			{
				tile.UpdateAdjacency(BuildAdjacency(tile));
			}
		}
		previewAffectedTiles = affectedTiles;
	}

	public void ClearPreviewAdjancency()
	{
		foreach(var tile in previewAffectedTiles)
		{
			tile.UpdateAdjacency(BuildAdjacency(tile));
		}
		previewAffectedTiles.Clear();
	}

	private void UpdateNeighbourAdjancency(Tile tile, List<Tile> affectedTiles = null, bool isPreview = false)
	{
		var tileSlot = new Vector2Int(GetGridX(tile.transform), GetGridZ(tile.transform));
		var neighbours = GenerateNeighbours(tileSlot.X, tileSlot.Z);
		foreach (var key in neighbours.Keys)
		{
			if (TilesContainsKey(key))
			{
				var neighbour = tiles[key.X][key.Z];
				neighbour.UpdateAdjacency(BuildAdjacency(neighbour, isPreview ? tile : null));
				if (affectedTiles != null)
				{
					affectedTiles.Add(neighbour);
				}
			}
		}
	}

	private int GetGridX(Transform transform)
	{
		return Mathf.RoundToInt(transform.position.x / this.GridSize);
	}

	private int GetGridZ(Transform transform)
	{
		return Mathf.RoundToInt(transform.position.z / this.GridSize);
	}

	private bool TilesContainsKey(Vector2Int tileSlot)
	{
		return tiles.ContainsKey(tileSlot.X) && tiles[tileSlot.X].ContainsKey(tileSlot.Z);
	}

	/// <summary>
	/// Generates Dictionary of tile position to direction to check
	/// </summary>
	/// <returns></returns>
	Dictionary<Vector2Int, Direction> GenerateNeighbours(int tileX, int tileZ)
	{
		return new Dictionary<Vector2Int, Direction> {
			{ new Vector2Int(tileX, tileZ - 1), Direction.Back },
			{ new Vector2Int(tileX - 1, tileZ), Direction.Left },
			{ new Vector2Int(tileX, tileZ + 1), Direction.Forward },
			{ new Vector2Int(tileX + 1, tileZ), Direction.Right },
		};
	}

	HashSet<Direction> BuildAdjacency(Tile tile, Tile previewTile = null)
	{
		var result = new HashSet<Direction>();
		var tileSlot = new Vector2Int(GetGridX(tile.transform), GetGridZ(tile.transform));
		var neighbours = GenerateNeighbours(tileSlot.X, tileSlot.Z);
		foreach (var key in neighbours.Keys)
		{
			if (TilesContainsKey(key)
				|| (previewTile != null
					&& key.X == GetGridX(previewTile.transform)
					&& key.Z == GetGridZ(previewTile.transform)
					&& AttachPointsMatch(tile, previewTile, neighbours[key])))
			{
				result.Add(neighbours[key]);
			}
		}
		return result;
	}

	Direction GetOppositeDirection(Direction direction)
	{
		return (Direction)(( ((int)direction) + 2 ) % 4);
	}
}
