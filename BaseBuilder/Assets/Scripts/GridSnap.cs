﻿using UnityEngine;
using System.Collections;

public class GridSnap : MonoBehaviour
{
	Grid grid;

	public void Init(Grid grid)
	{
		this.grid = grid;
	}

	public void SnapToGrid()
	{
		if (grid != null)
		{
			this.transform.position = new Vector3(
				Mathf.Round(this.transform.position.x / grid.GridSize) * grid.GridSize,
				0,
				Mathf.Round(this.transform.position.z / grid.GridSize) * grid.GridSize);
		}
	}

	void LateUpdate()
	{
		SnapToGrid();
	}
}
