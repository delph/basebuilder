﻿using UnityEngine;
using System.Collections;

public class HUD : MonoBehaviour
{
	public enum SelectedTool
	{
		None,
		Placer,
		Destroyer,
	}

	[SerializeField]
	float toolRange;
	[SerializeField]
	TilePlacer tilePlacer;
	[SerializeField]
	TileDestroyer tileDestroyer;
	[SerializeField]
	TriggerMonitor playerTriggerMonitor;

	SelectedTool currentTool;

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Alpha1))
		{
			SetSelectedTool(SelectedTool.Placer);
		}
		if (Input.GetKeyDown(KeyCode.Alpha2))
		{
			SetSelectedTool(SelectedTool.Destroyer);
		}
		if (Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Escape))
		{
			SetSelectedTool(SelectedTool.None);
		}
	}

	void OnGUI()
	{
		var buttonStyle = GUI.skin.button;
		GUILayout.BeginHorizontal();
		GUILayout.Toggle(currentTool == SelectedTool.Placer, "1. Creation Tool", buttonStyle);
		GUILayout.Toggle(currentTool == SelectedTool.Destroyer, "2. Destruction Tool", buttonStyle);
		GUILayout.Toggle(currentTool == SelectedTool.None, "3. No Tool", buttonStyle);
		GUILayout.EndHorizontal();
	}

	public void Init(Grid grid, Camera camera, Terrain terrain)
	{
		var terrainCollider = terrain.GetComponent<TerrainCollider>();
		tilePlacer.Init(grid, camera, terrainCollider, this.playerTriggerMonitor, toolRange);
		tileDestroyer.Init(camera, grid, terrainCollider, toolRange);
		SetSelectedTool(SelectedTool.Placer);
	}

	private void SetSelectedTool(SelectedTool tool)
	{
		tilePlacer.enabled = tool == SelectedTool.Placer;
		tileDestroyer.enabled = tool == SelectedTool.Destroyer;
		currentTool = tool;
	}
}
