﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Tile))]
public class ParentAssigner : MonoBehaviour
{
	[SerializeField]
	Transform[] assignToForwardParent;
	[SerializeField]
	Transform[] assignToRightParent;
	[SerializeField]
	Transform[] assignToBackParent;
	[SerializeField]
	Transform[] assignToLeftParent;

	void Awake()
	{
		var tile = this.GetComponent<Tile>();
		for(int i = 0, l = this.transform.childCount; i < l; i++)
		{
			this.transform.GetChild(i).parent = tile.GetParent(null);
		}
		AssignToDirectionalParent(assignToForwardParent, tile, Direction.Forward);
		AssignToDirectionalParent(assignToRightParent, tile, Direction.Right);
		AssignToDirectionalParent(assignToBackParent, tile, Direction.Back);
		AssignToDirectionalParent(assignToLeftParent, tile, Direction.Left);
	}

	void AssignToDirectionalParent(Transform[] transforms, Tile tile, Direction direction)
	{
		for(int i = 0, l = transforms.Length; i < l; i++)
		{
			transforms[i].parent = tile.GetParent(direction);
		}
	}
}
