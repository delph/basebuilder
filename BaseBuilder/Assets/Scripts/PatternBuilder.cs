﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Prototype Geometry Builder to Visualise Tile System
[RequireComponent(typeof(Tile))]
public class PatternBuilder : MonoBehaviour
{
	protected GameObject block { get { return Resources.Load("BuildingBlock") as GameObject; } }

	[SerializeField]
	public int Height = 4;

	// These are codes to give blocks the same parent so they can be easily switched on or off
	// Pattern Number - 2 = Direction Enum
	// (-1 => internal, include in ceiling)
	[SerializeField]
	public int[] Pattern = new int[36];

	Tile tile;

	void Awake()
	{
		tile = this.GetComponent<Tile>();
		Init();
	}

	void Init()
	{
		// Could borrow the mesh building code from Voxel Terrain
		for (int y = 0; y < Height; y++)
		{
			for (int x = 0; x < 6; x++)
			{
				for (int z = 0; z < 6; z++)
				{
					int key;
					if(y != 0 && y + 1 == Height)
					{
						// ceiling
						key = Pattern[x + z*6] != 0 ? 1 : 0;
					}
					else
					{
						key = Pattern[x + z*6];
					}

					if (key > 0)
					{
						Direction? direction = null;
						if (System.Enum.IsDefined(typeof(Direction), key - 2))
						{
							direction = (Direction)(key - 2);
						}
						Transform parent = tile.GetParent(direction);

						var go = Object.Instantiate(block) as GameObject;
						go.name = string.Format("Block ({0}, {1})", x, z);
						go.transform.parent = parent;
						go.transform.localPosition = new Vector3(2.5f - x, y, 2.5f - z);
					}

				}
			}
		}
	}
}
