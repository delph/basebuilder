﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum AttachPointType
{
	Empty,		// Implies no change from adjacency information
	Access,		// Implies wall removal
}

public enum Direction
{
	Forward,	// +z
	Right,	// +x
	Back,	// -z
	Left,	// -x
}

public class Tile : MonoBehaviour
{
	// Unity serialization is horrible, KISS solution
	[SerializeField]
	private AttachPointType forwardAttachPoint;
	[SerializeField]
	private AttachPointType rightAttachPoint;
	[SerializeField]
	private AttachPointType backAttachPoint;
	[SerializeField]
	private AttachPointType leftAttachPoint;

	private int rotation = 0; // Anti-Clockwise
	private Dictionary<Direction, AttachPointType> attachPoints;

	private Dictionary<Direction, Transform> directionalGeometryParents = new Dictionary<Direction, Transform>();
	private Transform nonDirectionalGeometryParent = null;

	private Material previewMaterial;
	private Dictionary<Renderer, Material> originalMaterials = new Dictionary<Renderer,Material>();

	[HideInInspector]
	public GridSnap GridSnap;

	private HashSet<Direction> adjacency = new HashSet<Direction>();

	void Awake()
	{
		attachPoints = new Dictionary<Direction, AttachPointType>();
		attachPoints[Direction.Forward] = forwardAttachPoint;
		attachPoints[Direction.Right] = rightAttachPoint;
		attachPoints[Direction.Back] = backAttachPoint;
		attachPoints[Direction.Left] = leftAttachPoint;

		previewMaterial = Resources.Load("PreviewMaterial") as Material;
	}

	public AttachPointType GetAttachPoint(Direction globalDirection)
	{
		return attachPoints[GetLocalDirection(globalDirection)];
	}

	private Direction GetLocalDirection(Direction globalDirection)
	{
		return (Direction)(((int)globalDirection + rotation) % 4);
	}

	private Direction GetGlobalDirection(Direction localDirection)
	{
		return (Direction)(((int)localDirection - rotation + 4) % 4);
	}

	public void Rotate(int antiClockwiseRightAngles)
	{
		rotation = (rotation + antiClockwiseRightAngles);
		while (rotation < 0)
		{
			rotation += 4;
		}
		rotation = rotation % 4;
		UpdateAdjacency(adjacency);
		this.transform.rotation = Quaternion.Euler(0, -rotation * 90, 0);
	}

	public Transform GetParent(Direction? direction)
	{
		Transform parent = this.transform;

		if ((direction.HasValue && !directionalGeometryParents.ContainsKey(direction.Value))
			|| (!direction.HasValue && nonDirectionalGeometryParent == null))
		{
			string parentName = "N/A";
			if (direction.HasValue)
			{
				parentName = direction.Value.ToString();
			}

			parent = (new GameObject(parentName)).transform;
			parent.parent = this.transform;
			parent.localPosition = Vector3.zero;
			if (direction.HasValue)
			{
				directionalGeometryParents.Add(direction.Value, parent);
			}
			else
			{
				nonDirectionalGeometryParent = parent;
			}
		}
		else
		{
			parent = direction.HasValue ? directionalGeometryParents[direction.Value] : nonDirectionalGeometryParent;
		}

		return parent;
	}

	public void AddGridSnap(Grid grid)
	{
		GridSnap = GridSnap ?? this.gameObject.AddComponent<GridSnap>();
		GridSnap.Init(grid);
	}

	public void AddTileCollider(float gridSize)
	{
		BoxCollider boxCollider = this.gameObject.GetComponent<BoxCollider>();
		if (boxCollider == null)
		{
			boxCollider = this.gameObject.AddComponent<BoxCollider>();
		}
		boxCollider.center = 0.5f * gridSize * Vector3.up;
		boxCollider.size = gridSize * Vector3.one;
		boxCollider.isTrigger = true;
	}

	public void PreviewTile()
	{
		SetPreviewMaterial();
		ToggleVisuals(true);
		ToggleColliders(false);
		UpdateAdjacency(adjacency);
	}

	public void PlaceTile()
	{
		RestoreMaterials();
		ToggleVisuals(true);
		ToggleColliders(true);
		UpdateAdjacency(adjacency);
	}

	private void ToggleVisuals(bool show)
	{
		if(nonDirectionalGeometryParent != null)
		{
			nonDirectionalGeometryParent.gameObject.SetActive(show);
		}
		foreach(var parent in directionalGeometryParents.Values)
		{
			parent.gameObject.SetActive(show);
		}
	}

	private void ToggleColliders(bool enable)
	{
		foreach(var collider in this.GetComponentsInChildren<Collider>())
		{
			collider.enabled = enable;
		}
	}

	public void UpdateAdjacency(HashSet<Direction> directions)
	{
		adjacency = directions;
		foreach (var localDirection in directionalGeometryParents.Keys)
		{
			if (directionalGeometryParents.ContainsKey(localDirection))
			{
				var go = directionalGeometryParents[localDirection].gameObject;
				if (attachPoints[localDirection] != AttachPointType.Empty)
				{
					// ^^ Arguably nothing should be defined under the parent
					// if the attach point type is "empty"
					go.SetActive(!directions.Contains(GetGlobalDirection(localDirection)));
				}
				else
				{
					go.SetActive(true);
				}
			}
		}
	}

	public void SetMaterialColour(Color colour, Color attachPointColour)
	{
		foreach (var renderer in this.nonDirectionalGeometryParent.GetComponentsInChildren<Renderer>())
		{
			renderer.material.color = colour;
		}
		foreach (var parent in this.directionalGeometryParents.Values)
		{
			foreach(var renderer in parent.GetComponentsInChildren<Renderer>())
			{
				renderer.material.color = attachPointColour;
			}
		}
	}

	public void EnableDeletionPreview(Color geometryTint, Color attachPointTint)
	{
		SetPreviewMaterial();
		SetMaterialColour(geometryTint, attachPointTint);
	}

	public void DisablePreviewDeletion()
	{
		RestoreMaterials();
	}

	private void SetPreviewMaterial()
	{
		foreach(var renderer in this.GetComponentsInChildren<Renderer>())
		{
			if(renderer.material != previewMaterial)
			{
				originalMaterials[renderer] = renderer.material;
				renderer.material = previewMaterial;
			}
		}
	}

	private void RestoreMaterials()
	{
		foreach(var renderer in originalMaterials.Keys)
		{
			renderer.material = originalMaterials[renderer];
		}
	}
}
