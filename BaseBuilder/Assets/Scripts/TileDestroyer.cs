﻿using UnityEngine;
using System.Collections;

public class TileDestroyer : MonoBehaviour
{
	[SerializeField]
	Color destructionTint = new Color(1f, 0, 0, 0.5f);
	[SerializeField]
	Color attachPointTint = new Color(0.5f, 0, 0.5f, 0.5f);

	float range;
	Camera targettingCamera;
	TerrainCollider terrianCollider;
	Grid grid;
	Tile currentTile;
	bool initialised;

	public void Init(Camera targettingCamera, Grid grid, TerrainCollider terrainCollider, float range)
	{
		this.range = range;
		this.targettingCamera = targettingCamera;
		this.grid = grid;
		this.terrianCollider = terrainCollider;
		initialised = true;
	}

	void OnGUI()
	{
		GUILayout.Space(30);
		GUILayout.Box("Left click to destroy tile.");
	}

	void Update()
	{
		if (!initialised)
		{
			return;
		}

		RaycastHit hitInfo;
		Ray ray = new Ray(targettingCamera.transform.position, targettingCamera.transform.forward);

		Tile tile = null;

		if (Physics.Raycast(ray, out hitInfo, range, 1 << LayerMask.NameToLayer("Tile")))
		{
			tile = hitInfo.collider.GetComponent<Tile>();
		}
		else if (terrianCollider.Raycast(ray, out hitInfo, range))
		{
			int gridX = Mathf.RoundToInt(hitInfo.point.x / grid.GridSize);
			int gridZ = Mathf.RoundToInt(hitInfo.point.z / grid.GridSize);

			grid.TryGetTile(gridX, gridZ, out tile);
		}

		if (tile != null)
		{
			if (tile != currentTile)
			{
				if (currentTile != null)
				{
					currentTile.DisablePreviewDeletion();
				}
				tile.EnableDeletionPreview(destructionTint, attachPointTint);
				currentTile = tile;
			}
		}
		else if (currentTile != null)
		{
			currentTile.DisablePreviewDeletion();
			currentTile = null;
		}

		if (currentTile != null && Input.GetMouseButtonDown(0))
		{
			grid.RemoveTile(currentTile);
		}
	}

	void OnDisable()
	{
		if (currentTile != null)
		{
			currentTile.DisablePreviewDeletion();
		}
	}
}
