﻿using UnityEngine;
using System.Collections;

public class TilePlacer : MonoBehaviour
{
	[SerializeField] Color validPlacementTint = new Color(83 / 255f, 1f, 0f, 0.5f);
	[SerializeField] Color invalidPlacementTint = new Color(1f, 0, 0, 0.5f);
	[SerializeField] Color attachPointTint = new Color(0, 0, 1, 0.5f);
	[SerializeField] string[] tilePrefabNames;

	float range = 50f;
	int selectedTileIndex;
	string displayedTileName;
	int overlappingColliders;

	Camera targettingCamera;
	TerrainCollider terrianCollider;
	TriggerMonitor playerTriggerMonitor;
	Grid grid;
	Tile tile;

	bool initialised;

	public void Init(
		Grid grid,
		Camera targettingCamera,
		TerrainCollider terrianCollider,
		TriggerMonitor playerTriggerMonitor,
		float range)
	{
		this.range = range;
		this.selectedTileIndex = 0;
		this.displayedTileName = tilePrefabNames[selectedTileIndex];
		this.grid = grid;
		this.targettingCamera = targettingCamera;
		this.terrianCollider = terrianCollider;
		initialised = true;

		if(this.playerTriggerMonitor != null)
		{
			this.playerTriggerMonitor.OnEnter -= OnPlayerTriggerEnter;
			this.playerTriggerMonitor.OnExit -= OnPlayerTriggerExit;
		}
		this.playerTriggerMonitor = playerTriggerMonitor;

		playerTriggerMonitor.OnEnter += OnPlayerTriggerEnter;
		playerTriggerMonitor.OnExit += OnPlayerTriggerExit;
	}

	void OnDisable()
	{
		grid.ClearPreviewAdjancency();
		if(tile != null)
		{
			Object.Destroy(tile.gameObject);
		}
	}

	void OnDestroy()
	{
		playerTriggerMonitor.OnEnter -= OnPlayerTriggerEnter;
		playerTriggerMonitor.OnExit -= OnPlayerTriggerExit;
	}

	void OnGUI()
	{
		GUILayout.Space(30);
		if(overlappingColliders == 0)
		{
			GUILayout.Box(string.Format(
				"Selected Tile: {0}\n{1}\n{2}\n{3}",
				tilePrefabNames[selectedTileIndex],
				"Scroll to change the tile to place",
				"Press R to rotate the tile",
				"Left click to place"));
		}
		else
		{
			GUILayout.Box("Placement disabled when inside occupied tiles");
		}
	}

	void Update()
	{
		if(!initialised || overlappingColliders > 0)
		{
			return;
		}

		if (tile == null || tilePrefabNames[selectedTileIndex] != displayedTileName)
		{
			if(tile != null)
			{
				Object.Destroy(tile.gameObject);
			}

			displayedTileName = tilePrefabNames[selectedTileIndex];
			var resource = Resources.Load(displayedTileName) as GameObject;
			tile = (Object.Instantiate(resource) as GameObject).GetComponent<Tile>();
			tile.AddGridSnap(grid);
			tile.PreviewTile();
		}

		RaycastHit hitInfo;
		Ray ray = new Ray(targettingCamera.transform.position, targettingCamera.transform.forward);

		Vector3? targetPosition = null;
		if(Physics.Raycast(ray, out hitInfo, range, 1 << LayerMask.NameToLayer("Tile")))
		{
			targetPosition = hitInfo.point + hitInfo.normal;
		}
		else if (terrianCollider.Raycast(ray, out hitInfo, range))
		{
			targetPosition = hitInfo.point;
		}

		if(targetPosition.HasValue)
		{
			tile.transform.position = targetPosition.Value;
			tile.GridSnap.SnapToGrid();

			if (!tile.gameObject.activeSelf)
			{
				tile.gameObject.SetActive(true);
			}
			if (grid.CanPlaceTile(tile))
			{
				tile.SetMaterialColour(validPlacementTint, attachPointTint);
			}
			else
			{
				tile.SetMaterialColour(invalidPlacementTint, Color.Lerp(invalidPlacementTint, attachPointTint, 0.5f));
			}
			grid.UpdatePreviewAdjancency(tile);
		}
		else
		{
			grid.ClearPreviewAdjancency();
			tile.gameObject.SetActive(false);
		}

		if(Input.GetMouseButtonDown(0))
		{
			if(tile.gameObject.activeSelf && grid.TryPlaceTile(tile))
			{
				tile.PlaceTile();
				tile = null;
			}
		}

		if(Input.GetKeyDown(KeyCode.R))
		{
			tile.Rotate(1);
		}

		if(Input.mouseScrollDelta != Vector2.zero)
		{
			int delta = Input.mouseScrollDelta.y > 0 ? 1 : -1;
			selectedTileIndex = (selectedTileIndex + delta) % tilePrefabNames.Length;
			while(selectedTileIndex < 0)
			{
				selectedTileIndex = tilePrefabNames.Length + selectedTileIndex;
			}
		}
	}

	private void OnPlayerTriggerExit(Collider collider)
	{
		if(collider.gameObject.layer == LayerMask.NameToLayer("Tile"))
		{
			overlappingColliders -= 1;
			UnityEngine.Assertions.Assert.IsTrue(this.overlappingColliders >= 0);

			if (overlappingColliders == 0 && tile != null)
			{
				tile.gameObject.SetActive(true);
			}
		}
	}

	private void OnPlayerTriggerEnter(Collider collider)
	{
		if(collider.gameObject.layer == LayerMask.NameToLayer("Tile"))
		{
			overlappingColliders += 1;
			if (tile != null)
			{
				tile.gameObject.SetActive(false);
			}
		}
	}
}
