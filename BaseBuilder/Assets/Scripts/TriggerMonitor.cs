﻿using UnityEngine;
using System.Collections;

public class TriggerMonitor : MonoBehaviour
{
	[SerializeField]
	bool logCollisions = false;
	public event System.Action<Collider> OnEnter;
	public event System.Action<Collider> OnStay;
	public event System.Action<Collider> OnExit;

	void OnTriggerEnter(Collider collider)
	{
		if (logCollisions)
		{
			Debug.Log("OnTriggerEnter: " + collider.name, this);
		}

		if (OnEnter != null)
		{
			OnEnter(collider);
		}
	}

	void OnTriggerExit(Collider collider)
	{
		if (logCollisions)
		{
			Debug.Log("OnTriggerExit: " + collider.name, this);
		}
		if (OnExit != null)
		{
			OnExit(collider);
		}
	}

	void OnTriggerStay(Collider collider)
	{
		if (OnStay != null)
		{
			OnStay(collider);
		}
	}

}
